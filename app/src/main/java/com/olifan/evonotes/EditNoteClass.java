package com.olifan.evonotes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

public class EditNoteClass extends AppCompatActivity {
    String noteText;
    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_details);
        Intent intent = getIntent();
        noteText = intent.getStringExtra(IntentConstants.INTENT_NOTE_DATA);
        position = intent.getIntExtra(IntentConstants.INTENT_ITEM_POSITION, -1);
        EditText noteData = (EditText) findViewById(R.id.note);
        noteData.setText(noteText);

    }

    @Override
    public void onBackPressed() {
        String changedNoteText = ((EditText)findViewById(R.id.note)).getText().toString();
        if(changedNoteText != null && !changedNoteText.trim().isEmpty()) {
            Intent intent = new Intent();
            intent.putExtra(IntentConstants.INTENT_CHANGED_NOTE, changedNoteText);
            intent.putExtra(IntentConstants.INTENT_ITEM_POSITION, position);
            setResult(IntentConstants.INTENT_RESULT_CODE_TWO, intent);
            finish();
        }else {
            Toast.makeText(this, "Заметка не может быть пустой", Toast.LENGTH_SHORT).show();
        }

        super.onBackPressed();
    }
}
