package com.olifan.evonotes;

public class IntentConstants {
    public final static int INTENT_REQUEST_CODE=1;
    public final static int INTENT_REQUEST_CODE_TWO=2;
    public final static int INTENT_RESULT_CODE=1;
    public final static int INTENT_RESULT_CODE_TWO=2;
    public final static String INTENT_NOTE_FIELD="note_field";
    public final static String INTENT_NOTE_DATA="note_data";
    public final static String INTENT_ITEM_POSITION="item_position";
    public final static String INTENT_CHANGED_NOTE="changed_note";
}
