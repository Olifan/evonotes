package com.olifan.evonotes;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    ArrayList<String> arrayList;
    ArrayAdapter<String> arrayAdapter;
    String noteText;
    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.notes_list);
        arrayList = new ArrayList<>();
        arrayAdapter = new ArrayAdapter<String>(this,R.layout.note_list_item, arrayList);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, EditNoteClass.class);
                intent.putExtra(IntentConstants.INTENT_NOTE_DATA,arrayList.get(i).toString());
                intent.putExtra(IntentConstants.INTENT_ITEM_POSITION, i);
                startActivityForResult(intent, IntentConstants.INTENT_REQUEST_CODE_TWO);

            }
        });
    }

    public void onClick(View view){
        Intent intent = new Intent();
        intent.setClass(MainActivity.this, NoteDetailsActivity.class);
        startActivityForResult(intent, IntentConstants.INTENT_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == IntentConstants.INTENT_REQUEST_CODE) {
            noteText = data.getStringExtra(IntentConstants.INTENT_NOTE_FIELD);
            arrayList.add(noteText);
            arrayAdapter.notifyDataSetChanged();
        } else if (resultCode==IntentConstants.INTENT_RESULT_CODE_TWO){
            noteText = data.getStringExtra(IntentConstants.INTENT_CHANGED_NOTE);
            position = data.getIntExtra(IntentConstants.INTENT_ITEM_POSITION,-1);
            arrayList.remove(position);
            arrayList.add(position, noteText);
            arrayAdapter.notifyDataSetChanged();
        }
    }
}
