package com.olifan.evonotes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

public class NoteDetailsActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_details);

    }

    @Override
    public void onBackPressed() {
        String noteText = ((EditText)findViewById(R.id.note)).getText().toString();
        if(noteText !=null && !noteText.trim().isEmpty()){
            Intent intent = new Intent();
            intent.putExtra(IntentConstants.INTENT_NOTE_FIELD, noteText);
            setResult(IntentConstants.INTENT_RESULT_CODE, intent);
            finish();
        }else {
            Toast.makeText(this, "Заметка не может быть пустой", Toast.LENGTH_SHORT).show();
        }

        super.onBackPressed();
    }
}
